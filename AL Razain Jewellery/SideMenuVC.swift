//
//  SideMenuVC.swift
//  AL Razain Jewellery
//
//  Created by Gabani King on 21/12/20.
//  Copyright © 2020 Ankit Gabani. All rights reserved.
//

import UIKit
import LGSideMenuController

class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewRound: UIView!
    
    var arrName = ["Home","Contact Us","Rings","Wedding Ring Band","Bracelets","Earrings","Pendants","Necklace Full Set","Available Discounts","Settings","Share"]
    
    var arrImage = ["ic_home","ic_call","oc_ri","ic_wedding","ic_braslate","icons8-earrings-100","ic_pend","icons8-necklace-100","icons8-discount-100","ic_setting","ic_share"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        viewRound.layer.cornerRadius = viewRound.frame.size.height / 2
        viewRound.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
           return .lightContent
       }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        
        cell.lblName.text = arrName[indexPath.row]
        cell.img.image = UIImage(named: arrImage[indexPath.row])
        cell.lblName.textColor = UIColor.darkGray
        cell.img.image = cell.img.image?.withRenderingMode(.alwaysTemplate)
        cell.img.tintColor = UIColor(red: 115/255, green: 115/255, blue: 115/255, alpha: 1)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        if indexPath.row == 0 {
            
            NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "1"])
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        } else if indexPath.row == 1 {
            NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "2"])

            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        } else if indexPath.row == 2 {
            NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "3"])

            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        } else if indexPath.row == 3 {
            NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "4"])

            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        } else if indexPath.row == 4 {
            NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "5"])

            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        } else if indexPath.row == 5 {
            NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "6"])

            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        } else if indexPath.row == 6 {
            NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "7"])

            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        } else if indexPath.row == 7 {
            NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "8"])

            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        } else if indexPath.row == 8 {
            NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "9"])

            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        } else if indexPath.row == 9 {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate

            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController

            let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
            navigation.viewControllers = [profileVC]
            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        } else if indexPath.row == 10 {
            NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": "11"])

            self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
        }
        
    }
    
}
