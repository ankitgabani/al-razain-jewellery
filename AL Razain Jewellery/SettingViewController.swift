//
//  SettingViewController.swift
//  AL Razain Jewellery
//
//  Created by Gabani King on 21/12/20.
//  Copyright © 2020 Ankit Gabani. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
           return .lightContent
       }
  
    @IBAction func back(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
            
            let viewcontrollers = self.navigationController?.viewControllers
            var isExist = false
            for viewcontroller in viewcontrollers! {
                if viewcontroller.isKind(of: ViewController.self) {
                    isExist = true
                    break
                }
            }
            if isExist == true {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.navigationController?.viewControllers.insert(controller, at: (viewcontrollers?.count)!)
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
    
}
