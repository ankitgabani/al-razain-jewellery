//
//  ViewController.swift
//  AL Razain Jewellery
//
//  Created by Gabani King on 21/12/20.
//  Copyright © 2020 Ankit Gabani. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD
import DropDown
import LGSideMenuController

extension Notification.Name {
    
    public static let myNotificationKey = Notification.Name(rawValue: "myNotificationKey")
}

class ViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var lbl1: UILabel!
    
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var lbl2: UILabel!
    
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var lbl3: UILabel!
    
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var lbl4: UILabel!
    
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var lbl5: UILabel!
    
    
    @IBOutlet weak var viewTarget: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tabBarView: UIView!
    var webView : WKWebView!
    
    var gradientLayer: CAGradientLayer!
    var gradientLayer1: CAGradientLayer!
    
    var arrOption = ["Settings","About","Share","Exit"]
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        createGradientLayer()
        //        createGradientLayer1()
        
        
        
        img1.image = img1.image?.withRenderingMode(.alwaysTemplate)
        img2.image = img2.image?.withRenderingMode(.alwaysTemplate)
        img3.image = img3.image?.withRenderingMode(.alwaysTemplate)
        img4.image = img4.image?.withRenderingMode(.alwaysTemplate)
        img5.image = img5.image?.withRenderingMode(.alwaysTemplate)
        
        img1.tintColor = UIColor.white
        
        img2.tintColor = UIColor.black
        img3.tintColor = UIColor.black
        img4.tintColor = UIColor.black
        img5.tintColor = UIColor.black
        
        
        webView = WKWebView()
        DispatchQueue.main.async {
            self.webView.frame = CGRect.init(x: 0, y: 0, width: self.mainView.frame.size.width, height: self.mainView.frame.size.height)
            
            self.webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/")! as URL) as URLRequest)
            
            self.webView.allowsBackForwardNavigationGestures = true
            
            self.webView.navigationDelegate = self
            
            self.webView.scrollView.bounces = false
            
            self.mainView.addSubview(self.webView)
        }
        
        
        setDropDownCountryCode()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.Name.myNotificationKey, object: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/")! as URL) as URLRequest)

        img1.tintColor = UIColor.white
        
        img2.tintColor = UIColor.black
        img3.tintColor = UIColor.black
        img4.tintColor = UIColor.black
        img5.tintColor = UIColor.black
    }
    
    //MARK:- Notification
    @objc func onNotification(notification:Notification)
    {
        
        let number = notification.userInfo!["text"] as? String
        
        if number == "1" {
            webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/")! as URL) as URLRequest)
            
        } else if number == "2" {
            
            if let phoneCallURL = URL(string: "telprompt://\(+965-98007901)") {
                
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        application.openURL(phoneCallURL as URL)
                        
                    }
                }
            }
            
        } else if number == "3" {
            webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/product-category/rings/")! as URL) as URLRequest)
            
        } else if number == "4" {
            webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/product-category/wedding-ring-band/")! as URL) as URLRequest)
            
        } else if number == "5" {
            webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/product-category/bracelets/")! as URL) as URLRequest)
            
        } else if number == "6" {
            webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/product-category/earrings/")! as URL) as URLRequest)
            
        } else if number == "7" {
            webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/product-category/pendants/")! as URL) as URLRequest)
            
        } else if number == "8" {
            webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/product-category/necklace-full-set/")! as URL) as URLRequest)
            
        } else if number == "9" {
            webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/")! as URL) as URLRequest)
            
        } else if number == "10" {
            webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/shop/")! as URL) as URLRequest)
            
        } else if number == "11" {
            
        } else if number == "12" {
            let urlStr = "itms-apps://itunes.apple.com/app/apple-store/id375380948?mt=8"
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                
            } else {
                UIApplication.shared.openURL(URL(string: urlStr)!)
            }
            
            
        }
        
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    func setDropDownCountryCode() {
        
        dropDown.dataSource = arrOption
        dropDown.anchorView = viewTarget
        dropDown.direction = .any
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if index == 0 {
                
                let setting = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
                self.navigationController?.pushViewController(setting, animated: true)
                
            } else if index == 1 {
                let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
                self.navigationController?.pushViewController(profileVC, animated: true)
                
            } else if index == 2 {
                let urlStr = "itms-apps://itunes.apple.com/app/apple-store/id375380948?mt=8"
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                    
                } else {
                    UIApplication.shared.openURL(URL(string: urlStr)!)
                }
            } else {
                exit(0)
            }
            
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: -viewTarget.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.white
        dropDown.backgroundColor = UIColor.black
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.selectedTextColor = UIColor.white
        
        
        dropDown.reloadAllComponents()
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.tabBarView.bounds
        
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        
        gradientLayer.colors = [UIColor(red: 61.0 / 255.0, green: 23.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0).cgColor, UIColor(red: 231.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0).cgColor]
        
        self.tabBarView.layer.addSublayer(gradientLayer)
    }
    
    func createGradientLayer1() {
        gradientLayer1 = CAGradientLayer()
        
        gradientLayer1.frame = self.topView.bounds
        
        gradientLayer1.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer1.endPoint = CGPoint(x: 1, y: 0)
        
        gradientLayer1.colors = [UIColor(red: 61.0 / 255.0, green: 23.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0).cgColor, UIColor(red: 231.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0).cgColor]
        
        self.topView.layer.addSublayer(gradientLayer1)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show()
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error)
        
    }
    
    
    @IBAction func clickedOption(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func clickedMenu(_ sender: Any) {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func clickedHome(_ sender: Any) {
        webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/")! as URL) as URLRequest)
        img1.tintColor = UIColor.white
        
        img2.tintColor = UIColor.black
        img3.tintColor = UIColor.black
        img4.tintColor = UIColor.black
        img5.tintColor = UIColor.black
        
    }
    
    @IBAction func clickedOrders(_ sender: Any) {
        webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/cart/")! as URL) as URLRequest)
        img2.tintColor = UIColor.white
        
        img1.tintColor = UIColor.black
        img3.tintColor = UIColor.black
        img4.tintColor = UIColor.black
        img5.tintColor = UIColor.black
        
    }
    
    
    @IBAction func clickedWishlist(_ sender: Any) {
        webView.load(NSURLRequest(url: NSURL(string: "https://alrazainjewellery.com/my-wishlist/")! as URL) as URLRequest)
        img3.tintColor = UIColor.white
        
        img2.tintColor = UIColor.black
        img1.tintColor = UIColor.black
        img4.tintColor = UIColor.black
        img5.tintColor = UIColor.black
        
    }
    
    
    @IBAction func clickedShare(_ sender: Any) {
        
        img4.tintColor = UIColor.white
        
        img2.tintColor = UIColor.black
        img3.tintColor = UIColor.black
        img1.tintColor = UIColor.black
        img5.tintColor = UIColor.black
        
        
        let urlStr = "itms-apps://itunes.apple.com/app/apple-store/id375380948?mt=8"
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
            
        } else {
            UIApplication.shared.openURL(URL(string: urlStr)!)
        }
    }
    
    @IBAction func clickedSettingd(_ sender: Any) {
        img5.tintColor = UIColor.white
        
        img2.tintColor = UIColor.black
        img3.tintColor = UIColor.black
        img4.tintColor = UIColor.black
        img1.tintColor = UIColor.black
        
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        self.navigationController?.pushViewController(profileVC, animated: true)
        
    }
    
    
    
}
