//
//  AppDelegate.swift
//  AL Razain Jewellery
//
//  Created by Gabani King on 21/12/20.
//  Copyright © 2020 Ankit Gabani. All rights reserved.
//

import UIKit
import LGSideMenuController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let home: ViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        let homeNavigation = UINavigationController(rootViewController: home)
        
        let leftViewController: SideMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        
        let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: leftViewController, rightViewController: nil)
        controller.leftViewWidth = 300

        homeNavigation.navigationBar.isHidden = true
        appDelegate.window?.rootViewController = controller
        appDelegate.window?.makeKeyAndVisible()
        
        return true
    }


}

